export const environment = {
  baseUrl: 'http://app.simplificandoar.com.br/api/',
  baseStorageUrl: 'http://app.simplificandoar.com.br/storage/',
  imageBaseUrl: 'https://mococa.s3-sa-east-1.amazonaws.com/',
  production: true
};
