import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/auth/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  PathRoutes = PathRoutes
  session = null

  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.session = this.sessionService.getSession()
  }

  logout() {
    this.sessionService.destroySession()
    this.router.navigate([PathRoutes.Login])
  }

}
