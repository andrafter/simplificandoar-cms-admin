import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit, Input } from '@angular/core';
import { SessionService } from 'src/app/auth/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  session = null
  PathRoutes = PathRoutes

  @Input() breadcrumbLink
  @Input() breadcrumbLinkName
  @Input() breadcrumbSubLink
  @Input() breadcrumbSubLinkName
  @Input() breadcrumbCurrent

  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.session = this.sessionService.getSession()
  }

  logout() {
    this.sessionService.destroySession()
    this.router.navigate([PathRoutes.Login])
  }
}
