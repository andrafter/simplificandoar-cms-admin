import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component } from '@angular/core';
import { SessionService } from './auth/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mococa-cms';
  notification: any = null
  session: any
  PathRoutes = PathRoutes
  environment = environment

  constructor(private sessionService: SessionService, private router: Router) {
    this.session = this.sessionService.getSession()
    if (this.session && this.session.version != environment.version) {
      this.sessionService.destroySession()
      this.router.navigate([PathRoutes.Login])
    }
  }
}
