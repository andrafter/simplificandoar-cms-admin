import { TestBed } from '@angular/core/testing';

import { LeavePageGuardService } from './leave-page-guard.service';

describe('LeavePageGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeavePageGuardService = TestBed.get(LeavePageGuardService);
    expect(service).toBeTruthy();
  });
});
