import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AppRoutingModule } from './routes/app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AdminsComponent } from './pages/admins/admins.component';
import { AdminsAddComponent } from './pages/admins/admins-add/admins-add.component';
import { AdminsDetailComponent } from './pages/admins/admins-detail/admins-detail.component';
import { AdminsChangePassComponent } from './pages/admins/admins-change-pass/admins-change-pass.component';
import { EstablishmentsComponent } from './pages/establishments/establishments.component';
import { EstablishmentsAddComponent } from './pages/establishments/establishments-add/establishments-add.component';
import { EstablishmentsDetailComponent } from './pages/establishments/establishments-detail/establishments-detail.component';
import { HomeComponent } from './pages/home/home.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrdersDetailComponent } from './pages/orders/orders-detail/orders-detail.component';
import { UsersComponent } from './pages/users/users.component';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/login/password-reset/password-reset.component';
import { UsersDetailComponent } from './pages/users/users-detail/users-detail.component';
import { FinancialComponent } from './pages/financial/financial.component';
import { RaffleComponent } from './pages/raffle/raffle.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { PasswordEmailComponent } from './pages/login/password-email/password-email.component';
import { FlatpickrComponent } from './components/flatpickr/flatpickr.component';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { EstablishmentsPasswordResetComponent } from './pages/establishments/establishments-password-reset/establishments-password-reset.component';
registerLocaleData(localePt);
import { CurrencyMaskModule, CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask";
import { PasswordResetSuccessComponent } from './pages/login/password-reset-success/password-reset-success.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { RequestsComponent } from './pages/requests/requests.component';
import { TermsComponent } from './pages/terms/terms.component';
import { TermsEditComponent } from './pages/terms/terms-edit/terms-edit.component';
import { RequestDetailComponent } from './pages/requests/request-detail/request-detail.component';
import { BudgetsComponent } from './pages/budgets/budgets.component';
import { ProvidersComponent } from './pages/providers/providers.component';
import { ProvidersDetailComponent } from './pages/providers/providers-detail/providers-detail.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductsDetailComponent } from './pages/products/products-detail/products-detail.component';
import { ProductsAddComponent } from './pages/products/products-add/products-add.component';
import { ManualsAddComponent } from './pages/manuals/manuals-add/manuals-add.component';
import { AdvantagesAddComponent } from './pages/advantages/advantages-add/advantages-add.component';
import { ProvidersAddComponent } from './pages/providers/providers-add/providers-add.component';
import { VideosComponent } from './pages/videos/videos.component';
import { VideosAddComponent } from './pages/videos/videos-add/videos-add.component';
import { VideosDetailComponent } from './pages/videos/videos-detail/videos-detail.component';
import { HelpComponent } from './pages/help/help.component';
import { BrandsComponent } from './pages/brands/brands.component';
import { BrandsAddComponent } from './pages/brands/brands-add/brands-add.component';
import { LeavePageGuardService } from './auth/leave-page-guard.service';
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "left",
  allowNegative: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    AdminsComponent,
    AdminsAddComponent,
    AdminsDetailComponent,
    AdminsChangePassComponent,
    EstablishmentsComponent,
    EstablishmentsAddComponent,
    EstablishmentsDetailComponent,
    HomeComponent,
    OrdersComponent,
    OrdersDetailComponent,
    UsersComponent,
    LoginComponent,
    PasswordResetComponent,
    UsersDetailComponent,
    FinancialComponent,
    RaffleComponent,
    EmptyStateComponent,
    PasswordEmailComponent,
    FlatpickrComponent,
    EstablishmentsPasswordResetComponent,
    PasswordResetSuccessComponent,
    NotFoundComponent,
    RequestsComponent,
    TermsComponent,
    TermsEditComponent,
    RequestDetailComponent,
    BudgetsComponent,
    ProvidersComponent,
    ProvidersDetailComponent,
    ProductsComponent,
    ProductsDetailComponent,
    ProductsAddComponent,
    ManualsAddComponent,
    AdvantagesAddComponent,
    ProvidersAddComponent,
    VideosComponent,
    VideosAddComponent,
    VideosDetailComponent,
    HelpComponent,
    BrandsComponent,
    BrandsAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(maskConfig),
    NgxDropzoneModule,
    FlatpickrModule.forRoot(),
    CurrencyMaskModule,
    CKEditorModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    LeavePageGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
