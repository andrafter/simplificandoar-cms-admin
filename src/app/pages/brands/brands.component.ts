import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { ProductsService } from 'src/app/services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements OnInit {


  columns = ['',]
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  btnLoading = false

  constructor(
    private productsService: ProductsService,
    public helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.getItems();
  }

  getItems() {
    this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.productsService.listBrands().then((res: any) => {
      if (res.status) {
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.title.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  edit(id) {
    this.router.navigate([PathRoutes.Brands.Edit + `/${id}`])
  }

}



