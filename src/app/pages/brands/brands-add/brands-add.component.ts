import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { BrandsService } from 'src/app/services/brands.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { ProductsService } from 'src/app/services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-brands-add',
  templateUrl: './brands-add.component.html',
  styleUrls: ['./brands-add.component.scss']
})
export class BrandsAddComponent implements OnInit {

  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  item
  form: FormGroup
  constructor(private formBuilder: FormBuilder, private helper: HelperService, private brandsService: BrandsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setupForm()
    this.getItem()
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "id": this.id,
      "title": ['', Validators.compose([Validators.required])],
      "picture": [null, !this.id ? Validators.compose([Validators.required]) : null],
    })
  }

  getItem() {
    if(this.id) {
      this.loading = true
      this.brandsService.get(this.id).then((res: any) => {
        if(res.status) {
          this.item = res.data
          this.form.patchValue(this.item)
          this.form.get('picture').setValue(null)
        } else {
          this.router.navigate([PathRoutes.NotFound])
        }
      })
      .catch(e => console.log(e))
      .finally(() => this.loading = false)
    }
  }
  onSelect(event) {
    this.form.get('picture').setValue(event.addedFiles[0])
    console.log(this.form.controls.picture.value)
  }
  onRemove() {
    this.form.get('picture').setValue(null)
  }
  async save() {
    try {
      this.loading = true
      const p = this.form.getRawValue()
      const res: any = await this.brandsService.save(p)
      if (res.status) {
        this.helper.triggerNotification(true);
        this.router.navigate([`${PathRoutes.Brands.List}`])
      } else {
        this.helper.triggerNotification(false);
        this.loading = false
      }
    } catch (e) { console.log(e) }
    this.loading = false
  }

}

