import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit {
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  item: any 

  constructor(private usersService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getItem()
  }

  getItem() {
    this.loading = true
    this.usersService.get(this.id).then((res: any) => {
      if(res.status) {
        this.item = res.data
      } else {
        this.router.navigate([PathRoutes.NotFound])
      }
        
    })
    .catch(e => console.log(e))
    .finally(() => this.loading = false)
  }

}
