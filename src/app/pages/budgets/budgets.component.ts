import { UsersService } from 'src/app/services/users.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { environment } from './../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from './../../helpers/helper.service';
import { Component, OnInit } from '@angular/core';
import { BudgetsService } from 'src/app/services/budgets.service';

@Component({
  selector: 'app-budgets',
  templateUrl: './budgets.component.html',
  styleUrls: ['./budgets.component.scss']
})
export class BudgetsComponent implements OnInit {

  columns = ['Nome', 'Título',  'Valor', 'Status', 'Data Criação']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  btnLoading = false

  constructor(
    private budgetsService: BudgetsService,
    public helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
      this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.budgetsService.list().then((res: any) => {
      if (res.status) {
        if (res.next_page_url) {
          this.btnLoading = true
        }
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.user.name.toLowerCase().includes(search.toLowerCase())
          || r.title.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  detail(item) {
    window.open(`${environment.baseStorageUrl}${item.path}`, "_blank");
  }

}
