import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvantagesAddComponent } from './advantages-add.component';

describe('AdvantagesAddComponent', () => {
  let component: AdvantagesAddComponent;
  let fixture: ComponentFixture<AdvantagesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvantagesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvantagesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
