import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { ProductsService } from 'src/app/services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-advantages-add',
  templateUrl: './advantages-add.component.html',
  styleUrls: ['./advantages-add.component.scss']
})
export class AdvantagesAddComponent implements OnInit {


  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  form: FormGroup
  constructor(private formBuilder: FormBuilder, private helper: HelperService, private productsService: ProductsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('product_id');
    this.setupForm()
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "product_id": [this.id, Validators.compose([Validators.required])],
      "title": ['', Validators.compose([Validators.required])],
      "description": ['', Validators.compose([Validators.required])],
      "picture": [null, Validators.compose([Validators.required])],
    })
  }

  onSelect(event) {
    this.form.get('picture').setValue(event.addedFiles[0])
    console.log(this.form.controls.picture.value)
  }
  onRemove() {
    this.form.get('picture').setValue(null)
  }
  async save() {
    try {
      this.loading = true
      const p = this.form.getRawValue()

      const res: any = await this.productsService.addAdvantage(p)
      if (res.status) {
        this.helper.triggerNotification(true);
        this.router.navigate([`${PathRoutes.Products.Detail}/${this.id}`])
      } else {
        this.helper.triggerNotification(false);
        this.loading = false
      }
    } catch (e) { console.log(e) }
    this.loading = false
  }

}

