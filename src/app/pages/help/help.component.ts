import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { HelpService } from 'src/app/services/help.service';
import { VideosService } from 'src/app/services/videos.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  columns = ['Usuário', 'Descricao', 'Categoria', 'Criado em']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  filters = {
    search: '',
    page: 1
  }
  btnLoading = false

  constructor(
    private helpService: HelpService,
    private helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
      this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.helpService.list().then((res: any) => {
      if (res.status) {
        if (res.next_page_url) {
          this.btnLoading = true
        }
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.description.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  detail(id) {
    this.router.navigate([PathRoutes.Videos.Detail + `/${id}`])
  }

}
