import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-providers-detail',
  templateUrl: './providers-detail.component.html',
  styleUrls: ['./providers-detail.component.scss']
})
export class ProvidersDetailComponent implements OnInit {
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  item: any

  constructor(private usersService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getItem()
  }

  getItem() {
    // this.loading = true
    this.usersService.get(this.id).then((res: any) => {
      console.log(res)
      if (res.status) {
        this.item = res.data
      } else {
        this.router.navigate([PathRoutes.NotFound])
      }

    })
      .catch(e => console.log(e))
      .finally(() => this.loading = false)
  }

  deActivate() {
    if (confirm('Deseja desativar esse fornecedor?')) {
      this.usersService.updateProvider({ id: this.id, active: 0 }).then(r => {
        this.getItem()
      })
    }
  }
  activate() {
    this.usersService.updateProvider({ id: this.id, active: 1 }).then(r => {
      this.getItem()
    })
  }

}


