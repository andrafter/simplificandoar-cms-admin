import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { ProductsService } from 'src/app/services/products.service';
import { ProvidersService } from 'src/app/services/providers.service';
import { UsersService } from 'src/app/services/users.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-providers-add',
  templateUrl: './providers-add.component.html',
  styleUrls: ['./providers-add.component.scss']
})
export class ProvidersAddComponent implements OnInit {


  environment = environment
  PathRoutes = PathRoutes
  loading = false
  files = []
  id
  item: any
  ufs = []

  form: FormGroup
  constructor(private formBuilder: FormBuilder, private userService: UsersService, private providersService: ProvidersService, private helper: HelperService, private productsService: ProductsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.ufs = this.helper.ufs
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setupForm()
    this.getItem()
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "id": [this.id],
      "type": ['provider'],
      "company_fantasy_name": ['', Validators.compose([Validators.required])],
      "company_name": ['', Validators.compose([Validators.required])],
      "company_cnpj": ['', Validators.compose([Validators.required])],
      "company_phone": ['', Validators.compose([Validators.required])],
      "company_state_registration": ['', Validators.compose([Validators.required])],
      "company_municipal_registration": [''],
      "company_email": ['', Validators.compose([Validators.required, Validators.required])],
      "company_email2": ['', Validators.compose([Validators.required, Validators.required])],
      "address_zip_code": ['', Validators.compose([Validators.required])],
      "address_street": ['', Validators.compose([Validators.required])],
      "address_number": ['', Validators.compose([Validators.required])],
      "address_neighborhood": ['', Validators.compose([Validators.required])],
      "address_city": ['', Validators.compose([Validators.required])],
      "address_state": ['', Validators.compose([Validators.required])],
      "address_complement": [''],
      "email": ['', Validators.compose([Validators.required, Validators.email])],
      "password": ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      "confirm_password": ['', Validators.compose([Validators.required])],
    })
  }

  getItem() {
    if(this.id) {
      this.loading = true
      this.userService.get(this.id).then((res: any) => {
        console.log(res)
        if(res.status) {
          this.item = res.data
          this.form.patchValue(this.item)
        } else {
          this.router.navigate([PathRoutes.NotFound])
        }
      })
      .catch(e => console.log(e))
      .finally(() => this.loading = false)
    }
  }



  cepComplete() {
    if (this.form.get('address_zip_code').value.length == 9) {
      return this.providersService.getCep(this.form.get('address_zip_code').value)
        .then(res => {


          this.form.patchValue({
            address_street: res.logradouro,
            address_neighborhood: res.bairro,
            address_city: res.localidade,
            address_state: res.uf,
            // complement: res.complemento
          });
        })
        .catch(error => {
          console.log(error)
          this.form.get('address_zip_code').setValue(null)
        });
    }
  }
  async save() {
    try {
      this.loading = true
      const p = this.form.getRawValue()
      const res: any = await this.providersService.save(p)

      if (res.status) {
        this.helper.triggerNotification(true);
        this.router.navigate([`${PathRoutes.Providers.Detail}/${res.data.id}`])
      } else {
        this.helper.triggerNotification(false,'Erro ao salvar', res.message);
        this.loading = false
      }
    } catch (e) { console.log(e) }
    this.loading = false
  }

}
