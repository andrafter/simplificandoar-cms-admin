import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from 'src/app/helpers/helper.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products-add',
  templateUrl: './products-add.component.html',
  styleUrls: ['./products-add.component.scss']
})
export class ProductsAddComponent implements OnInit, OnDestroy {
  errorMessage = ''
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  files = []
  routeSub
  id
  dataSaved = false
  copyFileImage
  copyParams
  item: any = {}
  manufacturers = []
  categories = []
  voltages = [
    {
      id: '110/1F',
      title: '110/1F'
    },
    {
      id: '220/1F',
      title: '220/1F'
    },
    {
      id: '380/3F',
      title: '380/3F'
    },
    {
      id: '220F/2F',
      title: '220F/2F'
    },
  ]
  technologies = [
    {
      id: 'INVERTER',
      title: 'INVERTER'
    },
    {
      id: 'CONVENCIONAL',
      title: 'CONVENCIONAL'
    },
  ]
  cycles = [
    {
      id: 'SÓ FRIO',
      title: 'SÓ FRIO'
    },
    {
      id: 'QUENTE E FRIO',
      title: 'QUENTE E FRIO'
    },
  ]
  energies = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
  compressor = ['Rotativo inverter', 'Duplo rotativo inverter']
  motors = ['CONVENCIONAL', 'DC INVERTER']

  form: FormGroup
  constructor(private formBuilder: FormBuilder, private helper: HelperService, private productsService: ProductsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setupForm()
    this.getItem()
    this.getManufacturers()
    this.getCategories()

  }

  errorMessages = {
    "manufacturer_id": 'Marca',
    "category_id": 'Tipo',
    "evaporator_code": 'Código da evaporadora',
    "condenser_code": 'Código da condensadora',
    "cooling_capacity": 'Capacidade de refrigeração',
    "cycle": 'Ciclo',
    "voltage": 'Tensão',
    "gas": 'Gás',
    "technology": 'Tecnologia',
    "energy_rating": 'Classificação energética',
    "internal_air_flow": 'Vazão de ar da evaporadora',
    "external_flow": 'Vazão de ar da condensadora ',
    "eer": 'EER',
    "cop": 'COP',
    "evaporator_net_weight": 'Peso da evaporadora',
    "condensing_net_weight": 'Peso da condensadora',
    "evaporator_height": 'Altura da evaporadora',
    "evaporator_width": 'Largura da evaporadora',
    "heating_capacity": 'Capacidade de aquecimento',
    "evaporator_depth": 'Comprimento da evaporadora',
    "condensing_height": 'Altura da condensadora',
    "condensing_width": 'Largura da condensadora',
    "condensing_depth": 'Comprimento da condensadora',
    "liquid_piping": 'Tubulação liquida',
    "gas_pipeline": 'Tubulação gás',
    "maximum_pipe_length": 'Comprimento max. do tudo',
    "maximum_height_difference": 'Desnível máximo',
    "maximum_internal_noise": 'Máximo ruído interno',
    "minimum_internal_noise": 'Minimo ruído interno',
    "external_noise": 'Ruído externo',
    "presence_sensor": 'Sensor de presença',
    "picture": 'Imagem',
  }


  setupForm() {
    this.form = this.formBuilder.group({
      "id": [this.id],
      "manufacturer_id": ['', Validators.compose([Validators.required])],
      "category_id": ['', Validators.compose([Validators.required])],
      "evaporator_code": ['', Validators.compose([Validators.required])],
      "condenser_code": ['', Validators.compose([Validators.required])],
      "cooling_capacity": ['', Validators.compose([Validators.required])],
      "heating_capacity": ['', Validators.compose([Validators.required])],
      "cycle": ['', Validators.compose([Validators.required])],
      "voltage": ['', Validators.compose([Validators.required])],
      "gas": ['', Validators.compose([Validators.required])],
      "technology": ['', Validators.compose([Validators.required])],
      "energy_rating": ['', Validators.compose([Validators.required])],
      "internal_air_flow": ['', Validators.compose([Validators.required])],
      "external_flow": ['', Validators.compose([Validators.required])],
      "input_power": [''],
      "eer": ['', Validators.compose([Validators.required])],
      "cop": ['', Validators.compose([Validators.required])],
      "evaporator_net_weight": ['', Validators.compose([Validators.required])],
      "condensing_net_weight": ['', Validators.compose([Validators.required])],
      "evaporator_height": ['', Validators.compose([Validators.required])],
      "evaporator_width": ['', Validators.compose([Validators.required])],
      "evaporator_depth": ['', Validators.compose([Validators.required])],
      "condensing_height": ['', Validators.compose([Validators.required])],
      "condensing_width": ['', Validators.compose([Validators.required])],
      "condensing_depth": ['', Validators.compose([Validators.required])],
      "liquid_piping": ['', Validators.compose([Validators.required])],
      "gas_pipeline": ['', Validators.compose([Validators.required])],
      "maximum_pipe_length": ['', Validators.compose([Validators.required])],
      "maximum_height_difference": ['', Validators.compose([Validators.required])],
      "maximum_internal_noise": ['', Validators.compose([Validators.required])],
      "minimum_internal_noise": ['', Validators.compose([Validators.required])],
      "external_noise": ['', Validators.compose([Validators.required])],
      "presence_sensor": ['Não', Validators.compose([Validators.required])],
      "compressor": ['Não possui'],
      "condenser_fan_motor": ['',],
      "evaporator_fan_motor": ['',],
      "picture": [null, !this.id ? Validators.compose([Validators.required]) : null],
      "picture_2": ['',],
      "picture_3": ['',],
    })
  }


  getItem() {
    if (this.id) {
      this.loading = true
      this.productsService.get(this.id).then((res: any) => {
        if (res.status) {
          this.item = res.data
          this.form.patchValue(this.item)
          this.form.get('picture').setValue(null)
          this.form.get('picture_2').setValue(null)
          this.form.get('picture_3').setValue(null)
        } else {
          this.router.navigate([PathRoutes.NotFound])
        }
      })
        .catch(e => console.log(e))
        .finally(() => this.loading = false)
    }
  }

  cycleChanged(event) {
    if (event.target.value == 'SÓ FRIO') {
      this.form.get('heating_capacity').disable()
      this.form.get('heating_capacity').setValue('')

    } else {
      this.form.get('heating_capacity').enable()
    }
  }


  async getManufacturers() {
    try {
      const r = await this.productsService.getManuFacturers()
      this.manufacturers = r.data.filter(s => { return { id: s.id, title: s.title } })
    } catch (e) { console.log(e) }

  }
  async getCategories() {
    try {
      const r = await this.productsService.getCategories()
      this.categories = r.data
    } catch (e) { console.log(e) }
  }
  onSelect(event, picFormControl) {
    this.form.get(picFormControl).setValue(event.addedFiles[0])
  }
  onRemove(picFormControl) {
    this.form.get(picFormControl).setValue(null)
  }
  getErrorMessage() {
    const keys = Object.keys(this.form.controls).filter(r => this.form.controls[r].errors)
    if (keys.length > 0) {
      return `O campo ${this.errorMessages[keys[0]]} é inválido`
    }
    return ''
  }
  async save() {

    try {
      this.loading = true
      const p = this.form.getRawValue()
      p.category = this.categories.filter(r => r.id == p.category_id)[0].title
      const res: any = await this.productsService.save(p)
      if (res.status) {
        this.dataSaved = true
        this.helper.triggerNotification(true);
        this.router.navigate([`${PathRoutes.Products.List}`])
      } else {
        this.helper.triggerNotification(false);
        this.loading = false
      }
    } catch (e) { console.log(e) }
    this.loading = false
  }

  ngOnDestroy() {

  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.dataSaved) {
      return confirm('Deseja sair desta pagina?\nTodos os dados não salvos serão perdidos')
    }
    return true
  }

}
