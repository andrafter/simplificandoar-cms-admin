import { UsersService } from 'src/app/services/users.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { environment } from './../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from './../../helpers/helper.service';
import { Component, OnInit } from '@angular/core';
import { BudgetsService } from 'src/app/services/budgets.service';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {


  columns = ['', 'Modelo', 'Manuais', 'Data Criação']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  btnLoading = false

  constructor(
    private productsService: ProductsService,
    public helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.productsService.list().then((res: any) => {
      if (res.status) {
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.description.toLowerCase().includes(search.toLowerCase())
          || r.manufacturer.title.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  detail(id) {
    this.router.navigate([PathRoutes.Products.Detail + `/${id}`])
  }

}



