import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import { HelperService } from 'src/app/helpers/helper.service';


@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements OnInit {
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  item: any

  constructor(private helper: HelperService, private productsService: ProductsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getItem()
  }

  getItem() {
    this.loading = true
    this.productsService.get(this.id).then((res: any) => {
      if (res.status) {
        this.item = res.data
      } else {
        this.router.navigate([PathRoutes.NotFound])
      }
    })
      .catch(e => console.log(e))
      .finally(() => this.loading = false)
  }

  removeManual(i) {
    const m = this.item.manuals[i]
    if (confirm('Deseja remover esse manual?')) {
      this.item.manuals.splice(i, 1)
      this.productsService.removeManual(m.id)
    }
  }
  removeAdvantage(i) {
    const a = this.item.advantages[i]
    if (confirm('Deseja remover essa vantagem?')) {
      this.item.advantages.splice(i, 1)
      this.productsService.removeAdvantage(a.id)
    }
  }

  async duplicate() {
    if (confirm('Deseja duplicar este produto?')) {
      const res: any = await this.productsService.duplicateProduct(this.id)
      if(res.status) {
        this.helper.triggerNotification(true, 'Produto criado', 'O produto foi duplicado com sucesso')
        this.router.navigate([PathRoutes.Products.List]);
      } else {
        this.helper.triggerNotification(false)
      }
    }
  }

}
