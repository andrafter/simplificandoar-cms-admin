import { HelperService } from 'src/app/helpers/helper.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { UsersService } from 'src/app/services/users.service';
import { AdminsService } from 'src/app/services/admins.service';

@Component({
  selector: 'app-admins-add',
  templateUrl: './admins-add.component.html',
  styleUrls: ['./admins-add.component.scss']
})
export class AdminsAddComponent implements OnInit {
  PathRoutes = PathRoutes
  form: FormGroup
  id = null
  loading = false;
  message

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private helper: HelperService,
    private usersService: UsersService,
    private adminsService: AdminsService
  ) { }

  
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.setupForm();

    if(this.id){
      this.getItem()
    }
  }

  getItem(){
    this.usersService.get(this.id).then((res: any) => {
      if(res.status){
        this.form.patchValue(res.data)
      }
    })
      .catch(e => console.log(e))
  }
  
  setupForm() {
    this.form = this.formBuilder.group({
      "id": this.id,
      "name": ['', Validators.compose([Validators.required])],
      "email":['', Validators.compose([Validators.required, Validators.email])],
      "password":['', Validators.compose(!this.id ? [Validators.required, Validators.minLength(8)] : [])],
      "confirm_password":[''],
      "type": 'admin',
    })
  }

  save(){
    this.loading = true

    if(!this.form.invalid){
      console.log(this.form.getRawValue())
      this.adminsService.save(this.form.getRawValue()).then((res:any) => {
        if(res.status){
          this.helper.triggerNotification(true);
          this.router.navigate([PathRoutes.Admins.List])
        } else {
          this.helper.triggerNotification(false);
          this.loading = false
        }        
      })
      .catch(e => {
        console.log(e)
        this.message = e.error.errors.email
        this.loading = false
      })
    }
  }

}
