import { AdminsService } from './../../../services/admins.service';
import { ActivatedRoute } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { HelperService } from 'src/app/helpers/helper.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admins-detail',
  templateUrl: './admins-detail.component.html',
  styleUrls: ['./admins-detail.component.scss']
})
export class AdminsDetailComponent implements OnInit {
  PathRoutes = PathRoutes
  id = null
  item: any

  constructor(
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    private helper: HelperService,
    private adminsService: AdminsService
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this.getItem()
    }
  }

  getItem() {
    this.usersService.get(this.id).then((res: any) => {
      if (res.status) {
        this.item = res.data
      }
    })
      .catch(e => console.log(e))
  }

  changeStatus(status) {
    this.helper.cancelSwal(status).then((result) => {
      if (result.value) {
        this.adminsService.save({id:this.id, active: status}).then((res: any) => {
          if(res.status){
            this.helper.triggerNotification(true, 'Alterado', 'Status alterado com sucesso')
            this.getItem()
          } else {
            this.helper.triggerNotification(false, 'Erro', 'Ocorreu algum erro, por favor tente novamente')
          }
        }).catch(e => console.log(e))
        
      }
    })    
  }
}
