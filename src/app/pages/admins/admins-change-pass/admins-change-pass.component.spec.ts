import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsChangePassComponent } from './admins-change-pass.component';

describe('AdminsChangePassComponent', () => {
  let component: AdminsChangePassComponent;
  let fixture: ComponentFixture<AdminsChangePassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsChangePassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsChangePassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
