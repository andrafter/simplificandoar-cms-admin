import { ActivatedRoute, Router } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { AdminsService } from 'src/app/services/admins.service';
import { UsersService } from 'src/app/services/users.service';
import { environment } from 'src/environments/environment';
import { HelperService } from 'src/app/helpers/helper.service';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {
  
  columns = ['Nome', 'Email', 'Data Criação']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  filters = {
    search: '',
    page: 1
  }
  btnLoading = false

  constructor(
    private usersService: UsersService,
    private helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.usersService.list('admin').then((res: any) => {
      if (res.status) {
        if (res.next_page_url) {
          this.btnLoading = true
        }
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.name.toLowerCase().includes(search.toLowerCase())
          || r.email.toLowerCase().includes(search.toLowerCase())
          || r.person_type.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  detail(id) {
    this.router.navigate([PathRoutes.Users.Detail + `/${id}`])
  }

}
