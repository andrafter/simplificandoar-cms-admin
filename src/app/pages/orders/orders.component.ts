import { HelperService } from 'src/app/helpers/helper.service';
import { Router } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  PathRoutes = PathRoutes
  columns = ['#', 'Cliente', 'Fornecedor', 'Status']
  loading = false
  loadingMore = false
  items = []
  filters = {
    search: '',
    page: 1
  }
  btnLoading = false

  constructor(
    private router: Router,
    private helper: HelperService,
    private ordersService: OrdersService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.getItems()
  }

  getItems(loadingMore = false) {

    if (!loadingMore) {
      this.filters.page = 1
      this.loading = true
    } else {
      this.loadingMore = true
    }

    this.ordersService.list({}).then((res: any) => {

      if (res.status) {

        this.btnLoading = res.next_page_url ? true : false

        if (loadingMore) {
          this.items = this.items.concat(res.data)
        } else {
          this.items = res.data
        }

      }

      this.filters.page++
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }

  loadMore() {
    this.getItems(true)
  }

  search(event) {
    this.filters.search = event.target.value
    this.getItems(false)
  }

  detail(id) {
    this.router.navigate([PathRoutes.Orders.Detail + `/${id}`])
  }

}
