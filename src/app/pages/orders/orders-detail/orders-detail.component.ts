import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { UsersService } from 'src/app/services/users.service';
import { environment } from 'src/environments/environment';
declare var $: any

@Component({
  selector: 'app-orders-detail',
  templateUrl: './orders-detail.component.html',
  styleUrls: ['./orders-detail.component.scss']
})
export class OrdersDetailComponent implements OnInit {
  PathRoutes = PathRoutes
  id = null
  item: any
  form: FormGroup
  columns = ['Produto', 'QTD', 'Valor unitário', 'Valor total', 'Pgto']
  products: any

  constructor(
    private ordersService: OrdersService,
    private activatedRoute: ActivatedRoute,
    public helper: HelperService,
    private usersService: UsersService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.getItem()
    this.formSetup()
  }

  detail(item) {
    window.open(`${environment.baseStorageUrl}${this.item.budget.path}`, "_blank");
  }

  getItem(){
    this.ordersService.get(this.id).then((res:any) => {
      if(res.status){
        this.item = res.data
        // this.usersService.get(res.data.budget.user_id).then((res: any) => {
        //   this.item.user = res.data
        // })
        var sum = 0
        this.item.products.map(r => {
            sum += Number(r.value) //* Number(r.qtd))
        })
        this.item.total_value = sum
      }
    })
  }

  formSetup() {
    this.form = this.formBuilder.group({
      'reason_for_cancellation': ['', Validators.compose([Validators.required])]
    })
  }

  changeStatus(status) {
    $('#modalCancel').modal('hide');
    let reason = this.form.getRawValue().reason_for_cancellation
    let params = { id: this.id, status: status, reason_for_cancellation: reason }
    console.log(params);
    this.ordersService.changeStatus(params).then((res: any) => {
      if (res.status) {
        this.getItem();
      }
    })
      .catch(e => console.log(e))
  }

}
