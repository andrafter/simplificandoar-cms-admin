import { PathRoutes } from 'src/app/routes/path-routes';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/helpers/helper.service';
import { FinancialService } from 'src/app/services/financial.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrdersService } from 'src/app/services/orders.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
declare var $: any
import { Router } from '@angular/router';


@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  columns = ['Estabelecimento', 'Status', 'Valor', '']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  selectedDate = '';
  items = []
  filters = {
    search: '',
    page: 1,
    dateFrom: '',
    dateTo: ''
  }
  form: FormGroup
  periods = ['Hoje', 'Ontem', 'Esta Semana', 'Este Mês']
  btnLoading = false
  currentReqIndex = -1

  constructor(private router: Router,private helper: HelperService, private financialService: FinancialService,
    private ordersService: OrdersService,
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getItems();
    this.formSetup()
  }

  getItems(loadingMore = false){

    if (!loadingMore) {
      this.filters.page = 1
      this.loading = true
    } else {
      this.loadingMore = true
    }

    this.financialService.list(this.filters).then((res: any) => {
      if(res.status){
        if(res.next_page_url){
          this.btnLoading = true
        }

        if(loadingMore){
          this.items = this.items.concat(res.data)
        } else {
          this.items = res.data
        }
      }
      this.filters.page++
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }

  formSetup() {
    this.form = this.formBuilder.group({
      'request_id': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])]
    })
  }


  loadMore() {
    this.getItems(true)
  }

  search(event) {
    this.filters.search = event.target.value
    this.getItems(false)
  }

  dateChange(event){
    var date = event.target.value.split(' ');
    this.filters.dateFrom = date[0];
    this.filters.dateTo = date[2];
    console.log(this.filters)
  }
  openRedeemModal(i) {
    this.currentReqIndex = i
    this.form.get('request_id').setValue(this.items[i].id)
    $("#redeemModal").modal()
  }
  periodChanged(event) {
    console.log(event.target.value)
    switch (event.target.value) {
      case "0": // "hoje"
        // this.filters.dateFrom = this.helper.getDateStringFromDate(new Date(), 'en-US')
        // this.filters.dateTo = this.helper.getDateStringFromDate(new Date(), 'en-US')
        break;

      case "1": // "ontem"
        var d = new Date()
        d.setDate(d.getDate() - 1)
        // this.filters.dateFrom = this.helper.getDateStringFromDate(d, 'en-US')
        // this.filters.dateTo = this.helper.getDateStringFromDate(d, 'en-US')
        break;
      case "2": // "esta semana"
        var today = new Date()
        var dateFrom = new Date()
        dateFrom.setDate(today.getDate() - today.getDay())
        var dateTo = new Date()
        dateTo.setDate(dateFrom.getDate() + 7)

        // this.filters.dateFrom = this.helper.getDateStringFromDate(dateFrom, 'en-US')
        // this.filters.dateTo = this.helper.getDateStringFromDate(dateTo, 'en-US')
        break;

      case "3": // "este mes"
        var today = new Date()

        var dateFrom = new Date(today.getFullYear(), today.getMonth(), 1)
        var dateTo = new Date(today.getFullYear(), today.getMonth() + 1, 0)

        // this.filters.dateFrom = this.helper.getDateStringFromDate(dateFrom, 'en-US')
        // this.filters.dateTo = this.helper.getDateStringFromDate(dateTo, 'en-US')
        break;
    }
    //this.getReport(true)
  }
  detail(id){
    this.router.navigate([PathRoutes.Requests.Detail+`/${id}`])
  }


}
