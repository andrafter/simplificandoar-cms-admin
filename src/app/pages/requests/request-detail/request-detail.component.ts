

import { AdminsService } from './../../../services/admins.service';
import { ActivatedRoute } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { HelperService } from 'src/app/helpers/helper.service';
import { FinancialService } from 'src/app/services/financial.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss']
})
export class RequestDetailComponent implements OnInit {
  PathRoutes = PathRoutes
  id = null
  item: any

  constructor(
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    private helper: HelperService,
    private adminsService: AdminsService,
    private financialService: FinancialService
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this.getItem()
    }
  }

  getItem() {
    this.financialService.get(this.id).then((res: any) => {
      console.log(res)
      if (res.status) {
        this.item = res.data
      }
    })
      .catch(e => console.log(e))
  }

  changeStatus(status) {

    var data = {
      id: this.item.id,
      status: status
    }

    this.financialService.save(data).then((res: any) => {
      this.getItem()
    })
      .catch(e => console.log(e))

  }
}
