import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualsAddComponent } from './manuals-add.component';

describe('ManualsAddComponent', () => {
  let component: ManualsAddComponent;
  let fixture: ComponentFixture<ManualsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
