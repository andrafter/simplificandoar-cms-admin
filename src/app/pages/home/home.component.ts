import { UsersService } from 'src/app/services/users.service';
import { OrdersService } from './../../services/orders.service';
import { EstablishmentsService } from 'src/app/services/establishments.service';
import { FinancialService } from 'src/app/services/financial.service';

import { AdminsService } from 'src/app/services/admins.service';
import { PathRoutes } from './../../routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { BudgetsService } from 'src/app/services/budgets.service';
import { ProductsService } from 'src/app/services/products.service';
import { ProvidersService } from 'src/app/services/providers.service';

declare var $ : any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  PathRoutes = PathRoutes
  productsCount = 0
  providersCount = 0
  budgetsCount = 0
  usersCount = 0
  ordersCount = 0
  constructor(
    private productsService: ProductsService,
    private budgetsService: BudgetsService,
    private usersService: UsersService,
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.productsService.list().then((res:any)=>{
      this.productsCount = res.data.length
    })
    this.budgetsService.list().then((res:any)=>{
      this.budgetsCount = res.data.length
    })
    this.ordersService.list({}).then((res:any)=>{
      this.ordersCount = res.data.length
    })
    this.usersService.list().then((res:any)=>{
      this.usersCount = res.data.length
    })
    this.usersService.list('provider').then((res:any)=>{
      this.providersCount = res.data.length
    })





  }

}
