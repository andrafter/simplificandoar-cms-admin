import { environment } from 'src/environments/environment';
import { PathRoutes } from 'src/app/routes/path-routes';
import { SessionService } from 'src/app/auth/session.service';
import { AccountService } from 'src/app/services/account.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup
  loading = false
  message = ''
  PathRoutes = PathRoutes
  environment = environment

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private sessionService: SessionService,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'password': [null, Validators.required]
    })
  }

  login() {

    this.loading = true
    this.message = ''
    this.accountService.login(this.form.getRawValue()).then((res: any) => {

      if (res.status && res.data.type == 'admin') {
        res.data.access_token = res.access_token
        res.data.version = environment.version
        this.sessionService.saveSession(res.data)
        this.router.navigate([PathRoutes.Home])
      } else {
        this.message = 'Usuário ou senha inválidos'
      }
    })
      .catch(e => console.log(e))
      .finally(() => { this.loading = false })
  }

  ngOnInit() {
  }

}
