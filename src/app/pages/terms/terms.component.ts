import { UsersService } from './../../services/users.service';
import { Component, OnInit } from '@angular/core';
import { PathRoutes } from '../../routes/path-routes'
@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent {
  PathRoutes = PathRoutes

  constructor(private usersService: UsersService) {
  }

}
