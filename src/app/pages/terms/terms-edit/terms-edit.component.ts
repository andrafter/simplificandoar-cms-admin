import { Component, OnInit } from '@angular/core';
import { PathRoutes } from 'src/app/routes/path-routes';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelperService } from 'src/app/helpers/helper.service';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-terms-edit',
  templateUrl: './terms-edit.component.html',
  styleUrls: ['./terms-edit.component.scss']
})
export class TermsEditComponent implements OnInit {
  public onReady(event: object) { }
  public onChange(event: object) { }
  public onFocus(event: object) { }
  public onBlur(event: object) { }
  public onFileUploadResponse(evt) { }
  public onFileUploadRequest(evt) { }

  PathRoutes = PathRoutes
  form: FormGroup
  loading = false
  type = ''
  terms: any = ''
  constructor(private activatedRoute: ActivatedRoute, public helper: HelperService, private formBuilder: FormBuilder, private usersService: UsersService) { }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.paramMap.get('type') == 'usuarios' ? 'user' : 'provider'
    this.setupForm()
    this.getItem()
  }


  async getItem() {
    const res = await this.usersService.getTerms()
    if(res.data.length > 0) {
      this.terms = res.data.filter(r => r.type == this.type)[0].terms_of_use
      this.form.patchValue({ terms_of_use: this.terms })
    }
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "terms_of_use": ['', Validators.compose([Validators.required])],
      "type": this.type
    })

  }

  save() {
    this.loading = true
    var params = this.form.getRawValue()
    if (!this.form.invalid) {
      this.usersService.saveTerms(params).then((res: any) => {
        this.loading = false
        if (res.status) {
          this.helper.triggerNotification(res.status)
        } else {
          this.helper.triggerNotification(res.status)
        }
      })
        .catch(e => { console.log(e); this.helper.triggerNotification(false) })
        .finally(() => { this.loading = false })

    }
  }
}
