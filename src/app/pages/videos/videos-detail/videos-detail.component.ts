import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { UsersService } from 'src/app/services/users.service';
import { VideosService } from 'src/app/services/videos.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-videos-detail',
  templateUrl: './videos-detail.component.html',
  styleUrls: ['./videos-detail.component.scss']
})
export class VideosDetailComponent implements OnInit {
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  id
  item: any
  url: SafeResourceUrl


  constructor(private helper: HelperService, private videosService: VideosService, private sanitizer: DomSanitizer, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getItem()
  }

  getItem() {
    // this.loading = true
    this.videosService.get(this.id).then((res: any) => {
      console.log(res)
      if(res.status) {
        this.item = res.data
        if(this.item.type == 'extra') {
          this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${this.item.video_id}`)
        } else {
          this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`https://player.vimeo.com/video/${this.item.video_id}`)
        }
        console.log(this.url)
      } else {
        this.router.navigate([PathRoutes.NotFound])
      }

    })
    .catch(e => console.log(e))
    .finally(() => this.loading = false)
  }

}
