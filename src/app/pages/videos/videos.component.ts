import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { UsersService } from 'src/app/services/users.service';
import { VideosService } from 'src/app/services/videos.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  columns = ['Titulo', 'Tipo', 'Criado em']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  loadingMore = false
  items = []
  filteredItems = []
  filters = {
    search: '',
    page: 1
  }
  btnLoading = false

  constructor(
    private videosService: VideosService,
    private helper: HelperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
      this.loading = true
    // if (!loadingMore) {
    //   this.filters.page = 1
    // } else {
    //   this.loadingMore = true
    // }

    this.videosService.list().then((res: any) => {
      if (res.status) {
        if (res.next_page_url) {
          this.btnLoading = true
        }
        this.items = res.data
        this.filteredItems = res.data
      }
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }



  search(event) {
    const search = event.target.value
    if (search == '') {
      this.filteredItems = this.items
    } else {
      this.filteredItems = this.items.filter(r => {
        return (
          r.title.toLowerCase().includes(search.toLowerCase())
        )
      })
    }
  }

  detail(id) {
    this.router.navigate([PathRoutes.Videos.Detail + `/${id}`])
  }

}
