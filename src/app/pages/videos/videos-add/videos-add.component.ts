import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/helpers/helper.service';
import { PathRoutes } from 'src/app/routes/path-routes';
import { ProductsService } from 'src/app/services/products.service';
import { VideosService } from 'src/app/services/videos.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-videos-add',
  templateUrl: './videos-add.component.html',
  styleUrls: ['./videos-add.component.scss']
})
export class VideosAddComponent implements OnInit {

  youtubeUrl = 'https://www.youtube.com/watch?v='
  vimeoUrl = 'https://www.vimeo.com/'
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  item
  id
  form: FormGroup

  types = [
    {
      id: 'courses',
      title: 'Cursos (Vimeo)'
    },
    {
      id: 'extra',
      title: 'Conteúdos extras (Youtube)'
    },
  ]
  constructor(private formBuilder: FormBuilder, private helper: HelperService, private videosService: VideosService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setupForm()
    this.getItem()
  }

  getItem() {
    if(this.id) {
      this.loading = true
      this.videosService.get(this.id).then((res: any) => {
        if(res.status) {
          this.item = res.data
          this.item.video_link = this.item.type == 'courses' ? `${this.vimeoUrl}${this.item.video_id}` : `${this.youtubeUrl}${this.item.video_id}`
          this.form.patchValue(this.item)
        } else {
          this.router.navigate([PathRoutes.NotFound])
        }
      })
      .catch(e => console.log(e))
      .finally(() => this.loading = false)
    }
  }
  setupForm() {
    this.form = this.formBuilder.group({
      "id": [this.id],
      "title": ['', Validators.compose([Validators.required])],
      "type": ['courses', Validators.compose([Validators.required])],
      "video_link": [null, Validators.compose([Validators.required])],
    })
  }

  async save() {
    // https://vimeo.com/411120384
    // https://www.youtube.com/watch?v=KGIM8MS9Alc
    try {
      var p = this.form.getRawValue()
      this.loading = true
      const res: any = await this.videosService.save(p)
      if (res.status) {
        this.helper.triggerNotification(true);
        this.router.navigate([`${PathRoutes.Videos.Detail}/${res.data.id}`])
      } else {
        this.helper.triggerNotification(false);
        this.loading = false
      }
    } catch (e) { console.log(e) }
    this.loading = false
  }



}

