import { environment } from 'src/environments/environment';
import { EstablishmentsService } from './../../services/establishments.service';
import { Router } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/helpers/helper.service';

@Component({
  selector: 'app-establishments',
  templateUrl: './establishments.component.html',
  styleUrls: ['./establishments.component.scss']
})
export class EstablishmentsComponent implements OnInit {
  PathRoutes = PathRoutes
  environment = environment
  columns = ['', 'Estabelecimento', 'Nome', 'Email', 'Status']
  loading = false
  loadingMore = false
  items = []
  filters = {
    search: '',
    page: 1
  }
  btnLoading = false
  
  constructor(
    private router: Router,
    private establishmentsService: EstablishmentsService,
    private helper: HelperService) { }

  ngOnInit() {
    this.getItems(false);
  }

  getItems(loadingMore = false){

    if (!loadingMore) {
      this.filters.page = 1
      this.loading = true
    } else {
      this.loadingMore = true
    }

    this.establishmentsService.list(this.filters).then((res: any) => {
      if(res.status){        
        if(res.next_page_url){
          this.btnLoading = true
        }

        if(loadingMore){
          this.items = this.items.concat(res.data)
        } else {
          this.items = res.data
        }
      }
      this.filters.page++
    })
      .catch(e => console.log(e))
      .finally(() => {
        this.loading = false
        this.loadingMore = false
      })
  }

  loadMore() {
    this.getItems(true)
  }

  search(event) {
    this.filters.search = event.target.value
    this.getItems(false)
  }

  detail(id){
    this.router.navigate([PathRoutes.Establishments.Detail+`/${id}`])
  }

}
