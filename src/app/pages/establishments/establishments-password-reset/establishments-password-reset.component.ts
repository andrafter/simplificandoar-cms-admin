import { HelperService } from 'src/app/helpers/helper.service';
import { Component, OnInit } from '@angular/core';
import { PathRoutes } from 'src/app/routes/path-routes';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EstablishmentsService } from 'src/app/services/establishments.service';
import { SessionService } from 'src/app/auth/session.service';

@Component({
  selector: 'app-establishments-password-reset',
  templateUrl: './establishments-password-reset.component.html',
  styleUrls: ['./establishments-password-reset.component.scss']
})
export class EstablishmentsPasswordResetComponent implements OnInit {

  PathRoutes = PathRoutes
  form: FormGroup
  loading = false
  id = null

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private establishmentsService: EstablishmentsService,
    private session: SessionService,
    private helper: HelperService
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.setupForm()
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "id": this.id,
      "old_password": ['', Validators.compose([Validators.required])],
      "password": ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      "confirm_password": ['']
    })
  }

  save() {
    if (!this.form.invalid) {
      this.establishmentsService.save(this.form.getRawValue()).then((res: any) => {
        if(res.status) {
          this.helper.triggerNotification(true);
          this.router.navigate([PathRoutes.Establishments.Detail+'/'+this.id])
        } else {
          this.helper.triggerNotification(false);
        }
      })
        .catch(e => console.log(e))
    }
  }

}
