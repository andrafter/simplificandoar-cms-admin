import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishmentsPasswordResetComponent } from './establishments-password-reset.component';

describe('EstablishmentsPasswordResetComponent', () => {
  let component: EstablishmentsPasswordResetComponent;
  let fixture: ComponentFixture<EstablishmentsPasswordResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstablishmentsPasswordResetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishmentsPasswordResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
