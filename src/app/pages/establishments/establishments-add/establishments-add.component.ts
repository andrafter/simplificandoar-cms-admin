import { environment } from 'src/environments/environment';
import { HelperService } from 'src/app/helpers/helper.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import Swal from 'sweetalert2';
import { EstablishmentsService } from 'src/app/services/establishments.service';

@Component({
  selector: 'app-establishments-add',
  templateUrl: './establishments-add.component.html',
  styleUrls: ['./establishments-add.component.scss']
})
export class EstablishmentsAddComponent implements OnInit {
  PathRoutes = PathRoutes
  environment = environment
  form: FormGroup
  bankForm: FormGroup
  id = null
  loading = false
  ufs = []
  files = []
  types = [
    'loja',
    'restaurante',
    'mercado',
    'lanchonete',
    'encomenda',
    'beleza',
    'pizzaria',
  ]
  hasLogo = false
  image_url: any
  item: any
  delivery_fee: any

  banksSelect = []


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    public helper: HelperService,
    private accountService: AccountService,
    private establishmentsService: EstablishmentsService
  ) {
    this.banksSelect = this.helper.banksArray
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.ufs = this.helper.ufs
    this.setupForm();

    if (this.id) {
      this.getItem()
      this.hasLogo = true
    }
  }

  bankChanged(event) {
    var code = event.target.value
    var name = ''
    if (code) {
      name = this.banksSelect.filter(r => r.code == code)[0].name
    }
    this.bankForm.get('bank').setValue(name)
  }

  setupForm() {
    this.form = this.formBuilder.group({
      "id": [this.id],
      "name": ['', Validators.compose([Validators.required])],
      "type": ['', Validators.compose([Validators.required])],
      "delivery_fee": [0],
      "image": [this.image_url],
      "email": ['', Validators.compose([Validators.email, Validators.required])],
      "password": ['', (!this.id ? Validators.compose([Validators.required, Validators.minLength(8)]) : '')],
      "confirm_password": [''],
      "address_street": ['', Validators.compose([Validators.required])],
      "address_number": ['', Validators.compose([Validators.required])],
      "address_city": ['', Validators.compose([Validators.required])],
      "address_cep": ['', Validators.compose([Validators.required, Validators.min(8)])],
      "address_neighborhood": ['', Validators.compose([Validators.required])],
      "address_state": ['', Validators.compose([Validators.required])],
    })
    this.bankForm = this.formBuilder.group({
      "id": '',
      "establishment_id": [this.id],
      "name": ['', Validators.compose([Validators.required])],
      "bank": ['', Validators.compose([Validators.required])],
      "cpf": ['', Validators.compose([Validators.required])],
      "agency": ['', Validators.compose([Validators.required])],
      "account_type": ['', Validators.compose([Validators.required])],
      "account_number": ['', Validators.compose([Validators.required])],
      "bank_code": ['', Validators.compose([Validators.required])],
      "agency_verifier": '',
      "account_verifier": '',
    })
  }

  async getItem() {
    try {
      const it = await this.establishmentsService.get(this.id)
      if (it.status) {
        if (this.id) {
          this.form.removeControl('password')
          this.form.removeControl('confirm_password')
        }
        this.form.patchValue(it.data)
        this.item = it.data
        // this.bankForm.patchValue(it.data.bank)
      }
    } catch (e) { console.log(e) }

  }

  cepComplete() {
    if (this.form.get('address_cep').value.length == 8) {
      return this.accountService.getCep(this.form.get('address_cep').value)
        .then(res => {

          var uf = this.ufs.filter(r => {
            return r.sigla == res.uf
          })

          this.form.patchValue({
            address_street: res.logradouro,
            address_neighborhood: res.bairro,
            address_city: res.localidade,
            address_state: uf[0].nome,
            // complement: res.complemento
          });
        })
        .catch(error => {
          console.log(error)
          this.form.get('address_cep').setValue(null)
        });
    }
  }

  activeEstablishment(id, active) {
    this.establishmentsService.active({ id: id, active: active }).then((res: any) => {
      if (res.status) {
        if (active) {
          this.helper.triggerNotification(true, 'Ativado', 'Estabelecimento ativado com sucesso');
        } else {
          this.helper.triggerNotification(true, 'Desativado', 'Estabelecimento desativado com sucesso')
        }
      } else {
        this.helper.triggerNotification(false, 'Erro', 'Ops, ocorreu algum erro!')
      }
      this.getItem()
    }).catch(e => console.log(e))
  }

  async save() {
    // this.loading = true

    // try {
    //   const r: any = await this.establishmentsService.save(this.form.getRawValue())
    //   if (r.status) {
    //     if (!this.id) {
    //       this.bankForm.get('establishment_id').setValue(r.data.id)
    //       const s: any = await this.establishmentsService.saveBank(this.bankForm.getRawValue())
    //       if (s.status) {
    //         this.helper.triggerNotification(true);
    //         this.router.navigate([PathRoutes.Establishments.List])
    //       } else {
    //         this.helper.triggerNotification(false);
    //         this.router.navigate([PathRoutes.Establishments.List])
    //       }
    //     } else {
    //       this.helper.triggerNotification(true);
    //       this.router.navigate([PathRoutes.Establishments.List])
    //     }
    //   } else {
    //     this.helper.triggerNotification(false);
    //     this.router.navigate([PathRoutes.Establishments.List])
    //   }
    // } catch (e) { console.log(e) }
  }

  onSelect(event) {
    this.files = event.addedFiles;
    this.image_url = (this.files[0])
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
    this.image_url = null;
  }

  cancelUpload() {
    this.hasLogo = !this.hasLogo
    this.files.splice(this.files.indexOf(event), 1);
    this.image_url = null;
    this.getItem()
  }

}
