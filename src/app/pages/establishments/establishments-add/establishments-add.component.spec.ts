import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishmentsAddComponent } from './establishments-add.component';

describe('EstablishmentsAddComponent', () => {
  let component: EstablishmentsAddComponent;
  let fixture: ComponentFixture<EstablishmentsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstablishmentsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishmentsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
