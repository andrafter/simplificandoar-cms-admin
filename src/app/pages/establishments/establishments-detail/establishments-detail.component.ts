import { environment } from 'src/environments/environment';
import { EstablishmentsService } from './../../../services/establishments.service';
import { ActivatedRoute } from '@angular/router';
import { PathRoutes } from 'src/app/routes/path-routes';
import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/helpers/helper.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-establishments-detail',
  templateUrl: './establishments-detail.component.html',
  styleUrls: ['./establishments-detail.component.scss']
})
export class EstablishmentsDetailComponent implements OnInit {
  PathRoutes = PathRoutes
  environment = environment
  id = null
  item: any
  delivery_fee

  constructor(
    private activcateRoute: ActivatedRoute,
    private establishmentsService: EstablishmentsService,
    public helper: HelperService
  ) { }

  ngOnInit() {
    this.id = this.activcateRoute.snapshot.paramMap.get('id');
    this.getItem()
  }

  getItem() {
    this.establishmentsService.get(this.id).then((res: any) => {
      if (res.status) {
        this.item = res.data
        this.delivery_fee = res.data.delivery_fee ? true : false
      }
    })
  }

  activeEstablishment(id, active) {
    this.helper.cancelSwal(active).then((result) => {
      if (result.value) {
        this.establishmentsService.active({ id: id, active: active }).then((res: any) => {
          if (res.status) {
            if (active) {
              this.helper.triggerNotification(true, 'Ativado', 'Estabelecimento ativado com sucesso');
            } else {
              this.helper.triggerNotification(true, 'Desativado', 'Estabelecimento desativado com sucesso')
            }
            this.getItem()
          } else {
            this.helper.triggerNotification(false, 'Erro', 'Ops, ocorreu algum erro!')
          }
        })
      }
    })
  }
}
