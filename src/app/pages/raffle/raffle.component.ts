import { PathRoutes } from 'src/app/routes/path-routes';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/helpers/helper.service';
import { RaffleService } from 'src/app/services/raffle.service';
import { Router } from '@angular/router';


declare var $: any


@Component({
  selector: 'app-raffle',
  templateUrl: './raffle.component.html',
  styleUrls: ['./raffle.component.scss']
})
export class RaffleComponent implements OnInit {
  columns = ['Nome', 'Email', 'Valor', 'Data']
  environment = environment
  PathRoutes = PathRoutes
  loading = false
  rafLoading = false

  loadingMore = false
  items = []
  filters = {
    search: '',
    page: 1
  }


  raffleVal = 300

  btnLoading = false

  constructor(private router: Router,private helper: HelperService, private raffleService: RaffleService) { }

  ngOnInit() {
    this.getItems();
  }

  getItems(loadingMore = false){

  }

  loadMore() {
    this.getItems(true)
  }

  search(event) {
    this.filters.search = event.target.value
    this.getItems(false)
  }



  runRaffle(){

  }



}
