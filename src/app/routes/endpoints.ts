
export const Endpoints = Object.freeze({
        Login: 'login',
        Users: 'users',
        PasswordReset: 'password/reset',
        PasswordEmail: 'password/email',
        Establishments: 'establishments',
        EstablishmentsUpdate: 'update/establishments',
        Orders: 'orders',
        Coupons: 'coupons',
        Financial: 'redemption-request',
        Transactions: 'transactions',
        Manuals: 'manuals',
        Advantages: 'advantages',
        Admins: 'admins',
        Terms: 'terms-of-use',
        Budgets: 'budgets',
        Products: 'products',
        Manufacturers: 'manufacturers',
        Videos: 'videos',
        Help: 'help',
        Categories: 'categories',
        DuplicateProduct: 'products/duplicate'



});
