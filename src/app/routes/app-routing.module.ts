import { TermsEditComponent } from './../pages/terms/terms-edit/terms-edit.component';
import { UsersDetailComponent } from './../pages/users/users-detail/users-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathRoutes } from './path-routes';
import { AuthService } from './../auth/auth.service';
import { PasswordResetComponent } from './../pages/login/password-reset/password-reset.component';
import { PasswordEmailComponent } from './../pages/login/password-email/password-email.component';
import { UsersComponent } from './../pages/users/users.component';
import { OrdersDetailComponent } from './../pages/orders/orders-detail/orders-detail.component';
import { OrdersComponent } from './../pages/orders/orders.component';
import { AdminsComponent } from './../pages/admins/admins.component';
import { AdminsAddComponent } from '../pages/admins/admins-add/admins-add.component';
import { AdminsDetailComponent } from './../pages/admins/admins-detail/admins-detail.component';
import { AdminsChangePassComponent } from './../pages/admins/admins-change-pass/admins-change-pass.component';
import { EstablishmentsComponent } from '../pages/establishments/establishments.component';
import { EstablishmentsAddComponent } from '../pages/establishments/establishments-add/establishments-add.component';
import { EstablishmentsDetailComponent } from '../pages/establishments/establishments-detail/establishments-detail.component';
import { HomeComponent } from './../pages/home/home.component';
import { LoginComponent } from '../pages/login/login.component';
import { FinancialComponent } from '../pages/financial/financial.component';
import { RaffleComponent } from '../pages/raffle/raffle.component';
import { EstablishmentsPasswordResetComponent } from '../pages/establishments/establishments-password-reset/establishments-password-reset.component';
import { PasswordResetSuccessComponent } from '../pages/login/password-reset-success/password-reset-success.component';
import { NotFoundComponent } from '../pages/not-found/not-found.component';
import { RequestsComponent } from '../pages/requests/requests.component';
import { TermsComponent } from '../pages/terms/terms.component';
import { RequestDetailComponent } from '../pages/requests/request-detail/request-detail.component';
import { BudgetsComponent } from '../pages/budgets/budgets.component';
import { ProvidersComponent } from '../pages/providers/providers.component';
import { ProvidersDetailComponent } from '../pages/providers/providers-detail/providers-detail.component';
import { ProductsComponent } from '../pages/products/products.component';
import { ProductsDetailComponent } from '../pages/products/products-detail/products-detail.component';
import { ProductsAddComponent } from '../pages/products/products-add/products-add.component';
import { ManualsAddComponent } from '../pages/manuals/manuals-add/manuals-add.component';
import { AdvantagesAddComponent } from '../pages/advantages/advantages-add/advantages-add.component';
import { ProvidersAddComponent } from '../pages/providers/providers-add/providers-add.component';
import { VideosAddComponent } from '../pages/videos/videos-add/videos-add.component';
import { VideosDetailComponent } from '../pages/videos/videos-detail/videos-detail.component';
import { VideosComponent } from '../pages/videos/videos.component';
import { HelpComponent } from '../pages/help/help.component';
import { BrandsComponent } from '../pages/brands/brands.component';
import { BrandsAddComponent } from '../pages/brands/brands-add/brands-add.component';
import { LeavePageGuardService } from '../auth/leave-page-guard.service';

const routes: Routes = [

  { path: PathRoutes.Login.replace('/', ''), component: LoginComponent},
  { path: PathRoutes.Password.Reset.replace('/', '')+'/:token', component: PasswordResetComponent},
  { path: PathRoutes.Password.Email.replace('/', ''), component: PasswordEmailComponent},
  { path: PathRoutes.Password.ResetSuccess.replace('/', ''), component: PasswordResetSuccessComponent},

  { path: '', component: HomeComponent, canActivate: [AuthService]},
  { path: PathRoutes.Home.replace('/', ''), component: HomeComponent, canActivate: [AuthService]},
  { path: PathRoutes.Budgets.List.replace('/', ''), component: BudgetsComponent, canActivate: [AuthService]},

  { path: PathRoutes.Admins.List.replace('/', ''), component: AdminsComponent, canActivate: [AuthService]},
  { path: PathRoutes.Admins.Add.replace('/', ''), component: AdminsAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Admins.Edit.replace('/', '')+'/:id', component: AdminsAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Admins.Detail.replace('/', '')+'/:id', component: AdminsDetailComponent, canActivate: [AuthService]},
  { path: PathRoutes.Admins.ChangePass.replace('/', '')+'/:id', component: AdminsChangePassComponent, canActivate: [AuthService]},

  { path: PathRoutes.Providers.List.replace('/', ''), component: ProvidersComponent, canActivate: [AuthService]},
  { path: PathRoutes.Providers.Add.replace('/', ''), component: ProvidersAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Providers.Edit.replace('/', '')+'/:id', component: ProvidersAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Providers.Detail.replace('/', '')+'/:id', component: ProvidersDetailComponent, canActivate: [AuthService]},

  { path: PathRoutes.Videos.List.replace('/', ''), component: VideosComponent, canActivate: [AuthService]},
  { path: PathRoutes.Videos.Add.replace('/', ''), component: VideosAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Videos.Edit.replace('/', '')+'/:id', component: VideosAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Videos.Detail.replace('/', '')+'/:id', component: VideosDetailComponent, canActivate: [AuthService]},

  { path: PathRoutes.Products.List.replace('/', ''), component: ProductsComponent, canActivate: [AuthService]},
  { path: PathRoutes.Products.Detail.replace('/', '')+'/:id', component: ProductsDetailComponent, canActivate: [AuthService]},
  { path: PathRoutes.Products.Add.replace('/', ''), component: ProductsAddComponent, canActivate: [AuthService],  canDeactivate: [LeavePageGuardService]},
  { path: PathRoutes.Products.Edit.replace('/', '')+'/:id', component: ProductsAddComponent, canActivate: [AuthService], canDeactivate: [LeavePageGuardService]},
  { path: PathRoutes.Products.AddManuals.replace('/', '')+'/:product_id', component: ManualsAddComponent, canActivate: [AuthService]},
  { path: PathRoutes.Products.AddAdvantages.replace('/', '')+'/:product_id', component: AdvantagesAddComponent, canActivate: [AuthService]},

  { path: PathRoutes.Orders.List.replace('/', ''), component: OrdersComponent, canActivate: [AuthService]},
  { path: PathRoutes.Orders.Detail.replace('/', '')+'/:id', component: OrdersDetailComponent, canActivate: [AuthService]},

  { path: PathRoutes.Users.List.replace('/', ''), component: UsersComponent, canActivate: [AuthService]},
  { path: PathRoutes.Users.Detail.replace('/', '')+'/:id', component: UsersDetailComponent, canActivate: [AuthService]},

  // { path: PathRoutes.Financial.replace('/', ''), component: FinancialComponent, canActivate: [AuthService]},
  // { path: PathRoutes.Requests.List.replace('/', ''), component: RequestsComponent, canActivate: [AuthService]},
  // { path: PathRoutes.Requests.Detail.replace('/', '')+'/:id', component: RequestDetailComponent, canActivate: [AuthService]},

  { path: PathRoutes.Help.replace('/', ''), component: HelpComponent, canActivate: [AuthService]},
  { path: PathRoutes.Terms.List.replace('/', ''), component: TermsComponent, canActivate: [AuthService] },
  { path: PathRoutes.Terms.Edit.replace('/', '')+'/:type', component: TermsEditComponent, canActivate: [AuthService] },

  { path: PathRoutes.Brands.List.replace('/', ''), component: BrandsComponent, canActivate: [AuthService] },
  { path: PathRoutes.Brands.Add.replace('/', ''), component: BrandsAddComponent, canActivate: [AuthService] },
  { path: PathRoutes.Brands.Edit.replace('/', '')+'/:id', component: BrandsAddComponent, canActivate: [AuthService] },


  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
