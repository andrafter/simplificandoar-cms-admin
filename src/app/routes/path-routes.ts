export const PathRoutes = Object({
  Home: '/home',
  Login: '/login',
  Password: {
    ResetSuccess: '/senha-alterada',
    Reset: '/password/reset',
    Email: '/esqueci-minha-senha',
  },
  Admins: {
    List: '/admins',
    Add: '/admins/adicionar',
    Edit: '/admins/editar',
    Detail: '/admins/detalhe',
    ChangePass: '/admins/alterar-senha'
  },
  Budgets: {
    List: '/orcamentos',
  },
  Products: {
    List: '/produtos',
    Detail: '/produtos/detalhe',
    Edit: '/produtos/editar',
    Add: '/produtos/adicionar',
    AddAdvantages: '/produtos/vantagens/adicionar',
    AddManuals: '/produtos/manuais/adicionar',
  },
  Establishments: {
    List: '/estabelecimentos',
    Add: '/estabelecimentos/adicionar',
    Edit: '/estabelecimentos/editar',
    Detail: '/estabelecimentos/detalhe',
    PasswordReset: '/estabelecimentos/alterar-senha'
  },
  Providers: {
    List: '/fornecedores',
    Detail: '/fornecedores/detalhe',
    Add: '/fornecedores/adicionar',
    Edit: '/fornecedores/editar',
  },
  Videos: {
    List: '/videos',
    Detail: '/videos/detalhe',
    Add: '/videos/adicionar',
    Edit: '/videos/editar',
  },
  Orders: {
    List: '/pedidos',
    Detail: '/pedidos/detalhe'
  },
  Brands: {
    List: '/fabricantes',
    Add: '/fabricantes/adicionar',
    Edit: '/fabricantes/editar'
  },
  Users: {
    List: '/usuarios',
    Detail: '/usuarios/detalhe',
  },
  Terms: {
    List: '/termos-de-uso',
    Edit: '/termos-de-uso/editar',
  },
  Financial: '/financeiro',
  Raffle: '/sorteios',
  Help: '/suporte',
  Requests: {
    List: '/requisicoes',
    Detail: '/requisicoes/detalhe'
  },
  NotFound: '/notfound',
});



