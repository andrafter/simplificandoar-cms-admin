import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';



@Injectable({
  providedIn: 'root'
})
export class BudgetsService {

  constructor(private network: NetworkService) { }

  list() {
    var url = `${Endpoints.Budgets}`
    return this.network.get(url)
  }
}
