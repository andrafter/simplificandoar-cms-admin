import { Endpoints } from './../routes/endpoints';
import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EstablishmentsService {

  constructor(private network: NetworkService) { }

  list(filters){
    var url = `${Endpoints.Establishments}?page=${filters.page}&search=${filters.search}`
    return this.network.get(url)
  }

  get(id){
    var url = `${Endpoints.Establishments}/${id}`
    return this.network.get(url)
  }

  save(info) {
    var params = info
    if(params.id) {
      var id = params.id
      delete(params.id)
      return this.network.postMultipart(`${Endpoints.EstablishmentsUpdate}/${id}`,params)
    } else {
      delete(params.id)
      return this.network.postMultipart(Endpoints.Establishments,params)
    }
  }

  active(params){
    var id = params.id
    delete(params.id)
    var url = `${Endpoints.Establishments}/${id}`
    return this.network.put(url, params)
  }



}
