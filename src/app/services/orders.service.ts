import { Endpoints } from './../routes/endpoints';
import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private network: NetworkService) { }

  list(filters: any = {}){
    var url = `${Endpoints.Orders}?`;
    if(filters.provider_id) {
      url += `provider_id=${filters.provider_id}`
    }
    return this.network.get(url)
  }

  get(id){
    var url = `${Endpoints.Orders}/${id}`
    return this.network.get(url);
  }

  changeStatus(params){
    var id = params.id
    delete(params.id)
    var url = `${Endpoints.Orders}/${id}`
    return this.network.put(url, params);
  }

}
