import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';



@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private network: NetworkService) { }

  list() {
    var url = `${Endpoints.Products}`
    return this.network.get(url)
  }
  listBrands() {
    var url = `${Endpoints.Manufacturers}`
    return this.network.get(url)
  }
  get(id) {
    var url = `${Endpoints.Products}/${id}`
    return this.network.get(url)
  }
  getManuFacturers() {
    var url = `${Endpoints.Manufacturers}`
    return this.network.get(url)
  }
  getCategories() {
    var url = `${Endpoints.Categories}`
    return this.network.get(url)
  }
  addManual(params){
    return this.network.postMultipart(`${Endpoints.Manuals}`, params)
  }
  addAdvantage(params){
    return this.network.postMultipart(`${Endpoints.Advantages}`, params)
  }
  removeAdvantage(id) {
    return this.network.delete(`${Endpoints.Advantages}/${id}`)
  }
  removeManual(id) {
    return this.network.delete(`${Endpoints.Manuals}/${id}`)
  }
  duplicateProduct(productId) {
    return this.network.post(`${Endpoints.DuplicateProduct}/${productId}`, {})
  }
  save(info) {
    if(info.id) {
      return this.update(info)
    }
    var params = {
      ...info,
      sku: `${info.condenser_code || ''}${info.evaporator_code || ''}`,
      description: `${info.category} ${info.technology} ${info.cooling_capacity} BTU/H ${info.cycle} ${info.voltage} ${info.gas}`
    }
    params.description = params.description.toUpperCase()
    params.gas = params.gas.toUpperCase()
    params.sku = params.sku.toUpperCase()

    var url = `${Endpoints.Products}`
    return this.network.postMultipart(url, params)
  }
  update(info) {
    const id = info.id
    var params = {
      ...info,
      description: `${info.category} ${info.technology} ${info.cooling_capacity} BTU/H ${info.cycle} ${info.voltage} ${info.gas}`
    }
    delete(params.id)
    params.description = params.description.toUpperCase()
    if(params.gas){
      params.gas = params.gas.toUpperCase()
    }
    console.log(params)
    var url = `${Endpoints.Products}/${id}`
    return this.network.postMultipart(url, params)
  }
}
