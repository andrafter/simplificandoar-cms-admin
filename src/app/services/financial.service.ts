import { Endpoints } from './../routes/endpoints';
import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {

  constructor(private network: NetworkService) { }

  list(filters){
    var url = `${Endpoints.Financial}`
    return this.network.get(url)
  }

  transactions(filters){
    var url = `${Endpoints.Transactions}` + '?id=' + filters.id + '&user_type=' + filters.user_type + '&type=' + filters.type + '&search=' + filters.search + '&dateFrom=' + filters.dateFrom + '&dateTo=' + filters.dateTo
    return this.network.get(url)
  }

  get(id){
    var url = `${Endpoints.Financial}/${id}`
    return this.network.get(url)
  }



  save(params) {
    if(params.id) {
      var id = params.id
      delete(params.id)
      return this.network.put(`${Endpoints.Financial}/${id}`, params)
    } else {
      delete(params.id)
      return this.network.post(Endpoints.Financial, params)
    }
  }

}
