import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class VideosService {


  constructor(private network: NetworkService) { }
  list() {
    return this.network.get(`${Endpoints.Videos}`)
  }
  get(id) {
    return this.network.get(`${Endpoints.Videos}/${id}`)
  }
  save(info) {
    var params = {
      ...info
    }
    delete(params.video_link)
    if(info.type == 'extra') {
      params.video_id = info.video_link.split('v=')[1]
    }
    if(info.type == 'courses') {
      const v = info.video_link.split('/')
      params.video_id = v[v.length-1]
    }
    if(params.id) {
      return this.update(params)
    }
    return this.network.post(`${Endpoints.Videos}`, params)
  }
  update(params) {
    const id = params.id
    delete(params.id)
    return this.network.put(`${Endpoints.Videos}/${id}`, params)
  }
}
