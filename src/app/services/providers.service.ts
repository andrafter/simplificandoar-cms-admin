import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';



@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(private network: NetworkService) { }
  getCep(cep) {
    return this.network.makeExternalRequest(`https://viacep.com.br/ws/${cep.replace('-', '')}/json/`, null, 'get')
  }
  save(params) {
    if(params.id) {
      return this.update(params)
    }

    return this.network.post(`${Endpoints.Users}`, params)
  }
  update(info) {
    const params = {
      ...info
    }
    const id = params.id
    delete(params.email)
    delete(params.password)
    delete(params.confirm_password)
    delete(params.id)
    return this.network.put(`${Endpoints.Users}/${id}`, params)
  }
}
