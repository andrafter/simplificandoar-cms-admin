import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';



@Injectable({
  providedIn: 'root'
})
export class BrandsService {

  constructor(private network: NetworkService) { }

  list() {
    var url = `${Endpoints.Manufacturers}`
    return this.network.get(url)
  }

  get(id) {
    var url = `${Endpoints.Manufacturers}/${id}`
    return this.network.get(url)
  }

  save(info) {
    if (info.id) {
      return this.update(info)
    }
    var params = {
      ...info
    }
    console.log(params)
    var url = `${Endpoints.Manufacturers}`
    return this.network.postMultipart(url, params)
  }
  update(info) {
    const id = info.id
    var params = {
      ...info,
    }
    delete (params.id)
    var url = `${Endpoints.Manufacturers}/${id}`
    return this.network.postMultipart(url, params)
  }
}
