import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Endpoints } from '../routes/endpoints';



@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private network: NetworkService) { }

  list(type = 'user') {
    var url = `${Endpoints.Users}`
    return new Promise((resolve, reject) => {
      this.network.get(url).then(r => {
        if(r.status) {
          var x = r.data.filter(it => {
              return it.type == type
          })
          r.data = x

        }
        resolve(r)
      })
      .catch(e => reject(e))
    })
  }
  listBudgets() {
    var url = `${Endpoints.Budgets}`
    return this.network.get(url)
  }
  getCep(cep) {
    return this.network.makeExternalRequest(`https://viacep.com.br/ws/${cep.replace('-', '')}/json/`, null, 'get')
  }
  saveProvider(params) {
    if(params.id) {
      return this.updateProvider(params)
    }
    return this.network.put(`${Endpoints.Users}`, params)
  }
  updateProvider(info) {
    const params = {
      ...info
    }
    const id = params.id
    delete(params.id)
    return this.network.postMultipart(`${Endpoints.Users}/${id}`, params)
  }
  get(id) {
  //   return new Promise((resolve, reject) => {
  //     resolve({
  //         "status": true,
  //         "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYwMjM4ODQ2NSwibmJmIjoxNjAyMzg4NDY1LCJqdGkiOiJLMWhWVEJ1VjJucXg2ODJSIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.F8rlCAcHSd7OGvmglh543TnkcZpYE0BnhIMn5BQ6EbQ",
  //         "data": {
  //             "id": 2,
  //             "type": "user",
  //             "person_type": "pj",
  //             "name": "Admin",
  //             "email": "admin@gmail.com",
  //             "phone": "(11)99226-2766",
  //             "cpf": "413.147.908-70",
  //             "birth": "1992-06-07",
  //             "email_verified_at": null,
  //             "company_name": "Teste ltda",
  //             "company_fantasy_name": "Teste",
  //             "company_email": "empresa@empresa.com",
  //             "company_phone": "(11)4132-0923",
  //             "company_cnpj": "54.274.760/0001-16",
  //             "company_state_registration": "278.246.630.580",
  //             "company_municipal_registration": "2786380",
  //             "address_street": "Chaim plat",
  //             "address_number": "89",
  //             "address_complement": null,
  //             "address_neighborhood": "Dos casa",
  //             "address_zip_code": "09840-510",
  //             "address_city": "São Bernardo do Campo",
  //             "address_state": "SP",
  //             "account_bank_name": "Nubank",
  //             "account_bank_code": "290",
  //             "account_agency": "00001",
  //             "account_type": "corrente",
  //             "account_number": "5120567",
  //             "responsible_name": "Andrews Alves",
  //             "responsible_cpf": "036.168.110-07",
  //             "responsible_birth": "1992-06-07",
  //             "responsible_phone": "(11)99209-9380",
  //             "responsible_address_street": "Rua Pedro de carvalho",
  //             "responsible_address_number": "23",
  //             "responsible_address_complement": "Apto B",
  //             "responsible_address_neighborhood": "Consolação",
  //             "responsible_address_zip_code": "01414-001",
  //             "responsible_address_city": "São Paulo",
  //             "responsible_address_state": "SP",
  //             "created_at": "2020-10-03T18:21:45.000000Z",
  //             "updated_at": "2020-10-05T20:53:37.000000Z"
  //         }
  //     })
  // })
    var url = `${Endpoints.Users}/${id}`
    return this.network.get(url)
  }

  getTerms() {
    return this.network.get(`${Endpoints.Terms}`)
  }
  saveTerms(params) {
    return this.network.post(`${Endpoints.Terms}`, params)
  }
}
